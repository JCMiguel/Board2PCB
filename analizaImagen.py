#!/usr/bin/python2
# -*- coding: utf-8 -*-

# Este archivo contiene las funciones de análisis de la imagen, como ser la detección de pads y pistas.

import cv2
import numpy as np



######################## Calculo de escalas #############################
# Calcula la escala de la imagen, expresada en pix/mm
#################################################################################
def CalculaEscala(imagen_gris, board_xmm, board_ymm):
	print("Calculando escalas...")
	ancho_imagen, alto_imagen = imagen_gris.shape[::-1]
	# Calculo de escalas.
	scale_x = ancho_imagen / board_xmm	# Indica a cuantos pixeles equivale 1mm.
	scale_y = alto_imagen / board_ymm	# Indica a cuantos pixeles equivale 1mm.
	return scale_x, scale_y
#---------------------------------------------------------------------------------







##################### Funcion de busqueda de contornos ##########################
# Genera listas y una imagen con los contornos (con y sin cuantificacion)
#################################################################################
def BuscaContornos (plantilla, imagen_binaria, imagen_binaria_cuantificada):

	# Nucleo para la transformacion de erosion.
	kernel = np.ones((5,5),np.uint8)
	imagen_binaria = cv2.erode(imagen_binaria,kernel,iterations = 1)
	#imagen_binaria_cuantificada = cv2.erode(imagen_binaria_cuantificada,kernel,iterations = 1)
	

	print("Buscando contornos...")
	lista_contornos, j_contornos = cv2.findContours(imagen_binaria, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)

	kernel = np.ones((5,5),np.uint8)
	imagen_binaria_cuantificada = cv2.erode(imagen_binaria_cuantificada,kernel,iterations = 2)
	#imagen_binaria_cuantificada = cv2.dilate(imagen_binaria_cuantificada,kernel,iterations = 2)
	lista_contornos_cuantificada, j_contornos_cuantificada = cv2.findContours(imagen_binaria_cuantificada, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
	
	aux = np.copy(plantilla)
	aux2 = cv2.cvtColor(aux.copy(), cv2.COLOR_GRAY2BGR)

	print("Generando imagen de contornos...")
	area_minima = 32
	for cnt in lista_contornos_cuantificada:
		area = cv2.contourArea(cnt)
		if area > area_minima:
			#color = np.random.randint(1,255,(3)).tolist()
			color = (127,127,127) #(192,192,192) 
			cv2.drawContours(aux, [cnt], -1, color, 5)

	#Aux2 sólo para debug!
	for cnt in lista_contornos:
		area = cv2.contourArea(cnt)
		if area > area_minima:
			color = np.random.randint(1,255,(3)).tolist()
			cv2.drawContours(aux2, [cnt], -1, color, 3)

	return aux2, aux, lista_contornos, lista_contornos_cuantificada
#---------------------------------------------------------------------------------




########################### Funcion de deteccion de islas ###################################
def DetectaPads (img_7pix):
	# Nucleo para la transformacion de erosion.
	kernel = np.ones((2,2),np.uint8)
	img_7pix = cv2.dilate(img_7pix,kernel,iterations = 3)
	img_7pix = cv2.GaussianBlur(img_7pix, (7, 7), sigmaX = 2, sigmaY = 2)
	#cimg = cv2.cvtColor(img_7pix,cv2.COLOR_GRAY2BGR)

	print("DetectaPads: Buscando circulos")
	### 1 32 50 19 7 30	No sirve para pads grandes.
	### Valores para plantilla3.png -> Espesor de contorno de 7 pixeles.
	circles = cv2.HoughCircles(img_7pix, cv2.cv.CV_HOUGH_GRADIENT, 1, 32, param1=50, param2=19, minRadius=7, maxRadius=30)
	
	print("DetectaPads: Redondeando resultados...")
	circles = np.uint16(np.around(circles))

	#print("DetectaPads: Graficando resultados")
	#for i in circles[0,:]:
	    # draw the outer circle
	#    cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
	    # draw the center of the circle
	#    cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),3)
		#i[0] e i[1] son x e y (respectivamente?); i[2] es el radio del circulo.
	#cv2.imwrite('Pads.png',cimg)

	return circles, img_7pix
#------------------------------------------------------------------------------------





######################## TITLE #############################
# 
#################################################################################
def DetectaPistas (lista_islas, lista_contornos_cuantificada, scale_x, scale_y, plantilla, imagen_binaria_cuantificada):
	print("Filtrando islas en el circuito...")
	imagen_sinpads = PreProcesoPistas(lista_islas,imagen_binaria_cuantificada)
	#ret,th_bi = cv2.threshold(gray, cv2.THRESH_OTSU, 255, cv2.THRESH_BINARY)
	#th_bi = cv2.erode(th_bi,kernel,iterations = 1)
	#contornos,jerarquia = cv2.findContours(th_bi, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
	
	#plantilla = cv2.cvtColor(plantilla,cv2.COLOR_GRAY2BGR)

	pixeles = imagen_sinpads.size	# Calculo auxiliar, cantidad de pixeles en la imagen.
	copy_x = 0		# Variable para copiar el valor de la abscisa x.
	copy_y = 0		# Variable para copiar el valor de la ordenada y.
	xi = [0]		# Lista de ceros. Aqui se guardan las abscisas iniciales de las rectas.
	yi = xi[:]		# Idem... Aqui se guardan las ordenadas iniciales de las rectas.
	xf = xi[:]		# Idem... Aqui se guardan las abscisas finales de las rectas.
	yf = xi[:]		# Idem... Aqui se guardan las ordenadas finales de las rectas.
	hor = 0

	area_minima = 32
	area_maxima = 0.025*pixeles #0.01 * pixeles
	plantilla_filtro = np.copy(plantilla)
	print("Comenzando deteccion de rectas...")
	print("Esto puede durar unos minutos.")
	for cnt in lista_contornos_cuantificada:
		area = cv2.contourArea(cnt)
		if (area < area_maxima) and (area > area_minima):
			#cv2.fillPoly(plano_byw, [cnt], color=(255,255,255))
			cv2.fillPoly(plantilla_filtro, [cnt], color=(255,255,255))

	# Pasado el filtro, busco las rectas.
	xip,yip,xfp,yfp,elem = DetectaRectas(plantilla_filtro, scale_x, scale_y, grid_mm=(0.25+0.125/2), lmin_mm=1)
	if (elem != 0):
		xi = np.append(xi,xip)
		yi = np.append(yi,yip)
		xf = np.append(xf,xfp)
		yf = np.append(yf,yfp)
	else:
		print("Ninguna recta detectada!")

	return xi,yi,xf,yf
#---------------------------------------------------------------------------------





######################## TITLE #############################
# 
#################################################################################
def BuscaPlano(lista_contornos, imagen_ref):
	print("Buscando plano de masa...")
	ancho_imagen, alto_imagen = imagen_ref.shape[::-1]

	# Valores iniciales en caso de no deteccion de plano.
	x = 0
	y = 0
	w = ancho_imagen
	h = alto_imagen

	area_plano = 0.4 * imagen_ref.size
	for cnt in lista_contornos:
		area = cv2.contourArea(cnt)
		if (area > area_plano):
			color = np.random.randint(0,255,(3)).tolist()
			x,y,w,h = cv2.boundingRect(cnt)
			break

	return x,y,w,h
#---------------------------------------------------------------------------------








########################### Procesamiento principal ####################################
def DetectaRectas (img, scale_x, scale_y, grid_mm, lmin_mm):#, tope_izq, tope_sup, ancho_ventana, alto_ventana):
	(alto,ancho) = img.shape
	lmin = int((lmin_mm*(scale_x + scale_y))/2)

	### Preparacion de variables. ###
	i = 0;			# Variable auxiliar, indice.
	objetivo = 255		# Color a buscar.
	#pixeles = ancho_ventana * alto_ventana	# Calculo auxiliar, cantidad de pixeles en el tramo de imagen bajo analisis.
	pixeles = ancho * alto
	copy_x = 0		# Variable para copiar el valor de la abscisa x.
	copy_y = 0		# Variable para copiar el valor de la ordenada y.
	xi = [0] * int(pixeles * 0.7)	# Lista de ceros. Aqui se guardan las abscisas iniciales de las rectas.
	yi = xi[:]		# Idem... Aqui se guardan las ordenadas iniciales de las rectas.
	xf = xi[:]		# Idem... Aqui se guardan las abscisas finales de las rectas.
	yf = xi[:]		# Idem... Aqui se guardan las ordenadas finales de las rectas.
	#horizontal = 0

	### Aca viene el algoritmo de deteccion. ###
	# Barrido Vertical.

	#if not (tope_izq < pad_x < tope_der) and not (tope_sup < pad_y < tope_inf):
	#if (pad_x < tope_izq or pad_x > tope_der) and (pad_y < tope_sup or pad_y > tope_inf):
	#if (tope_izq < pad_x < tope_der) and (tope_sup < pad_y < tope_inf):
	print("DetectaRectas: Buscando rectas verticales...")
	for x in range(1, ancho, int(grid_mm*scale_x)):
		for y in range(1, alto, int(grid_mm*scale_y)):
			#print(x,y)
			if (img[y,x] == objetivo) and (xi[i] == 0) and (yi[i] == 0):
				xi[i] = x
				yi[i] = y
				#print("Encontre un inicio de recta")
			if (img[y,x] == objetivo) and (xi[i] != 0) and (yi[i] != 0):
				xf[i] = x
				yf[i] = y
				#print("Buscando")
			if (img[y,x] != objetivo) and (xi[i] != 0) and (yi[i] != 0):
				if (yf[i] - yi[i] > lmin-1):
					i += 1
					#print("Aca termino la recta")
				else:
					xi[i] = 0
					yi[i] = 0

	# Guardo el valor de cambio. Me permite ahorrar espacio en memoria.
	# Uso las listas x e y tanto para rectas verticales como para horizontales.
	# El valor de "cambio" me indica desde que punto empiezo a guardar los extremos de las rectas horizontales.
	# Ver implementacion en etapa de graficado de rectas, dentro del range.
	horizontal = i

	# Barrido Horizontal
	print("DetectaRectas: Buscando rectas horizontales...")
	for y in range(1, alto, int(grid_mm*scale_y)):
		for x in range(1, ancho, int(grid_mm*scale_x)):
			if (img[y,x] == objetivo) and (xi[i] == 0) and (yi[i] == 0):
				xi[i] = x
				yi[i] = y
			if (img[y,x] == objetivo) and (xi[i] != 0) and (yi[i] != 0):
				xf[i] = x
				yf[i] = y
			if (img[y,x] != objetivo) and (xi[i] != 0) and (yi[i] != 0):
				if (xf[i] - xi[i] > lmin-1):
					i += 1
				else:
					xi[i] = 0
					yi[i] = 0

	# Guardo el valor de cambio. Me permite ahorrar espacio en memoria.
	# Ahora empiezo a buscar diagonales ascendentes!
	# Ver implementacion en etapa de graficado de rectas, dentro del range.
	diagonal_a = i

	#Barrido Diagonal /45
	print("DetectaRectas: Buscando diagonales ascendentes a 45 grados...")
	x = 1
	for y in range(1, alto-1, int(grid_mm*scale_y)):
		copy_x = x
		copy_y = y
		while (copy_y > 0) and (copy_x < ancho):
			#print(copy_x,copy_y)
			if (img[copy_y,copy_x] == objetivo) and (xi[i] == 0) and (yi[i] == 0):
				xi[i] = copy_x
				yi[i] = copy_y
				#print("Encontre un inicio de recta")
			if (img[copy_y,copy_x] == objetivo) and (xi[i] != 0) and (yi[i] != 0):
				xf[i] = copy_x
				yf[i] = copy_y
				#print("Buscando")
			if (img[copy_y,copy_x] != objetivo) and (xi[i] != 0) and (yi[i] != 0):
				if (int(1.4142*(yi[i] - yf[i])) > lmin-1):
					i += 1
					#print("Aca termino la recta")
				else:
					xi[i] = 0
					yi[i] = 0
			copy_x += int(grid_mm*scale_x)
			copy_y -= int(grid_mm*scale_y)
	if y > alto-1:
		y -= int(grid_mm*scale_y)
	tope = y
	for x in range(1, ancho-1, int(grid_mm*scale_x)):
		copy_x = x
		copy_y = y
		while (copy_y > 0) and (copy_x < ancho):
			#print(copy_x,copy_y)
			if (img[copy_y,copy_x] == objetivo) and (xi[i] == 0) and (yi[i] == 0):
				xi[i] = copy_x
				yi[i] = copy_y
				#print("Encontre un inicio de recta", copy_x,copy_y)
			if (img[copy_y,copy_x] == objetivo) and (xi[i] != 0) and (yi[i] != 0):
				xf[i] = copy_x
				yf[i] = copy_y
				#print("Buscando")
			if (img[copy_y,copy_x] != objetivo) and (xi[i] != 0) and (yi[i] != 0):
				if (int(1.4142*(yi[i] - yf[i])) > lmin-1):
					i += 1
					#print("Aca termino la recta")
				else:
					xi[i] = 0
					yi[i] = 0
			copy_x += int(grid_mm*scale_x)
			copy_y -= int(grid_mm*scale_y)

	# Guardo el valor de cambio. Me permite ahorrar espacio en memoria.
	# Ahora empiezo a buscar diagonales descendentes!
	# Ver implementacion en etapa de graficado de rectas, dentro del range.
	diagonal_d = i

	#Barrido Diagonal \
	print("DetectaRectas: Buscando diagonales descendentes a 45 grados...")
	x = 1
	for y in range(tope, 1, -int(grid_mm*scale_y)):
		copy_x = x
		copy_y = y
		while (copy_y < alto) and (copy_x < ancho):
			#print(copy_x,copy_y)
			if (img[copy_y,copy_x] == objetivo) and (xi[i] == 0) and (yi[i] == 0):
				xi[i] = copy_x
				yi[i] = copy_y
				#print("Encontre un inicio de recta")
			if (img[copy_y,copy_x] == objetivo) and (xi[i] != 0) and (yi[i] != 0):
				xf[i] = copy_x
				yf[i] = copy_y
				#print("Buscando")
			if (img[copy_y,copy_x] != objetivo) and (xi[i] != 0) and (yi[i] != 0):
				if (int(1.4142*(yf[i] - yi[i])) > lmin-1):
					i += 1
					#print("Aca termino la recta")
				else:
					xi[i] = 0
					yi[i] = 0
			copy_x += int(grid_mm*scale_x)
			copy_y += int(grid_mm*scale_y)
	if y < 1:
		y += int(grid_mm*scale_y)
	for x in range(1, ancho-1, int(grid_mm*scale_x)):
		copy_x = x
		copy_y = y
		while (copy_y < alto) and (copy_x < ancho):
			#print(copy_x,copy_y)
			if (img[copy_y,copy_x] == objetivo) and (xi[i] == 0) and (yi[i] == 0):
				xi[i] = copy_x
				yi[i] = copy_y
				#print("Encontre un inicio de recta", copy_x,copy_y)
			if (img[copy_y,copy_x] == objetivo) and (xi[i] != 0) and (yi[i] != 0):
				xf[i] = copy_x
				yf[i] = copy_y
				#print("Buscando")
			if (img[copy_y,copy_x] != objetivo) and (xi[i] != 0) and (yi[i] != 0):
				if (int(1.4142*(yf[i] - yi[i])) > lmin-1):
					i += 1
					#print("Aca termino la recta")
				else:
					xi[i] = 0
					yi[i] = 0
			copy_x += int(grid_mm*scale_x)
			copy_y += int(grid_mm*scale_y)

	#Optimizo memoria!
	print("DetectaRectas: Evaluando datos significativos...")
	xi_opt = [0] * i
	yi_opt = xi_opt[:]
	xf_opt = xi_opt[:]
	yf_opt = xi_opt[:]
	for j in range(0,i):
		xi_opt[j] = xi[j]
		yi_opt[j] = yi[j]
		xf_opt[j] = xf[j]
		yf_opt[j] = yf[j]

	return (xi_opt,yi_opt,xf_opt,yf_opt,i)
#------------------------------------------------------------------------------------






########################### Funcion de deteccion de pistas ###################################
def PreProcesoPistas(pads,raw):
	print("PreProcesoPistas: Tapando vias...")
	for i in pads[0,:]:
		cv2.circle(raw,(i[0],i[1]),2,(255,0,0),17)
		#cv2.circle(raw,(i[0],i[1]),i[2]/2,(0,0,0),i[2])
	return raw
#------------------------------------------------------------------------------------
