Instrucciones de uso
--------------------

Este código permite tomar la imagen de una cámara conectada a la computadora, de un video en formato compatible (por ejemplo AVI) o de una imagen fija.

Para tomar los datos de la cámara (en este caso la cámara 0), ejecutar el comando:

	python Board2PCB_v1.py -v 0

También puede especificarse la cámara 1. En caso de querer tomar los datos cargando un video en formato AVI, se debe indicar la ruta donde se encuentra el archivo, por ejemplo:

	python Board2PCB_v1.py -v MarcaAzul.avi

Tanto si se toman los datos de la cámara como del video, se debe indicar al programa en qué momento tomar la captura para procesar y buscar los pads, pistas y plano de masa. Para esto, debe pulsarse la tecla F en el momento en que se recuadren la placa impresa (en color verde) y la insignia patrón (en color rojo). Ésta es una forma gráfica de indicar cuando el algoritmo es capaz de procesar la imagen adecuadamente. Si falta alguno de los recuadros en pantalla, se descarta la captura.

Si desea usarse simplemente una imagen para probar el algoritmo, deberá usarse el argumento -i seguido de la ruta y el nombre del archivo imagen, junto con las dimensiones de la placa impresa expresadas en milímetros mediante los argumentos -l y -a.

	python Board2PCB_v1.py -i W108x82.png -l 108 -a 82

Las imágenes provistas para probar el algoritmo incluyen en su nombre las dimensiones que deben cargarse con los argumentos -l y -a. Ejemplo: W108x82.png => -l 108 -a 82.
En la versión de código del año pasado resultó necesario usar una etapa de preprocesamiento que hacía una cuantización de la imagen, reduciendo así el ruido en la misma. Al usar una cámara nueva, esta etapa acabó siendo destructiva para la captura. Por tal motivo, en esta versión esa etapa es opcional, y en caso de usarse deberá indicarse el argumento -p.

	python Board2PCB_v1.py -v MarcaCian.avi -p 64

Donde 64 es un valor umbral utilizado para la cuantización.
Se incorporó también una función de DEBUG que permite guardar en imágenes PNG los resultados parciales de cada etapa del código. Ejemplo:

	python Board2PCB_v1.py -v 1 --debug

Puede consultarse una descripción breve de cada argumento usando -h.

	python Board2PCB_v1.py -h

ENJOY!
