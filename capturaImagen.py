#!/usr/bin/python2
# -*- coding: utf-8 -*-


import imutils
import cv2
import numpy as np


########################### Funcion principal ###################################
def ObtieneImagen (filepath):

	# Umbrales mínimo y máximo de color para la insignia patrón.
	lower = np.array([150,30,0], dtype = "uint8")
	upper = np.array([255,170,90], dtype = "uint8")

	# Inicialización de cámara.
	print("Este código recuadra la placa en color VERDE y la insignia patrón (cuadrado color azul) en ROJO.")
	print("Para procesar una captura, presione la tecla F.")
	print("Si al presionar la tecla F no están ambos recuadros en pantalla, la imagen no será procesada.")
	print("\nCargando imagen...")
	if(filepath == "0"):
		filepath = int(filepath)
	elif(filepath == "1"):
		filepath = int(filepath)
	
	cap = cv2.VideoCapture(filepath) #1)
	hayVideo = 0

	while(True):
		# Factor empírico, para compensar el achicamiento de la imagen por la cámara.
		fcam = 1.063 #.033922419 

		# Borrado de datos intermedios.
		placaVisible = False # Este es un flag que me indica que veo la placa.
		insigniaVisible = False # Flag que me indica que veo la insignia.
		endStream = False # Flag que indica si hay datos de video entrantes.

		ret, raw = cap.read()
		if(ret):
			hayVideo = 1
			raw_copy = raw.copy()
			gray_img = cv2.cvtColor(raw,cv2.COLOR_BGR2GRAY)
			th_img = cv2.adaptiveThreshold(gray_img, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 255, 1)
			(alto_imagen, ancho_imagen) = gray_img.shape

			# Preparando plantilla en negro.
			size_p = alto_imagen, ancho_imagen #añadir ,3 para imagen de tres canales.
			plantilla=np.zeros(size_p,dtype=np.uint8)

			# Buscando contornos.
			contornos,jerarquia = cv2.findContours(th_img.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
			area_ref = 0.35 * alto_imagen * ancho_imagen

			for cnt in contornos:
				area = cv2.contourArea(cnt)
				if (area > area_ref):
					perimetro = cv2.arcLength(cnt, True)
					approx = cv2.approxPolyDP(cnt, 0.02 * perimetro, True)
					if len(approx) == 4:
						cv2.drawContours(raw, [approx], -1, (0,255,0), 1)
						placaVisible = True
						mask = cv2.inRange(raw, lower, upper)
						insignia = cv2.bitwise_and(raw, raw, mask = mask)
						th_warp = cv2.adaptiveThreshold(cv2.cvtColor(insignia,cv2.COLOR_BGR2GRAY), 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 255, 1)
						conti,jerar = cv2.findContours(th_warp, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
						for cnt3 in conti:
							area3 = cv2.contourArea(cnt3)
							if ((area3 < 0.20*500000) and (area3 > 150)): #58250):
								peri3 = cv2.arcLength(cnt3, True)
								approx3 = cv2.approxPolyDP(cnt3, 0.02 * peri3, True)
								if len(approx3) == 4:
									cv2.drawContours(raw, [approx3], -1, (0,0,255) ,2)
									insigniaVisible = True

			cv2.imshow("Visualización de circuito impreso",raw)
			key = cv2.waitKey(1) & 0xFF
			if ((key == ord('f')) or (key == ord('F'))):
				cap.release()
				cv2.destroyAllWindows()
				break
		else:
			if(hayVideo == 1):
				cap = cv2.VideoCapture(filepath) #1)
			else:
				print("No hay ingreso de datos.")
				endStream = True
				exit()
				break
	
	if(endStream is False):
		print("Captura realizada")
		if(placaVisible and insigniaVisible):
			warped=np.zeros((233,350,3),dtype=np.uint8)

			print("Buscando perspectiva.")
			src = np.array(approx, dtype="float32")
			src3 = src.copy()
			dst = np.array([[0,0],[640,0],[640,640],[0,640]], dtype="float32")
			M = cv2.getPerspectiveTransform(src,dst)
	
			print("Modificando.")
			warped = cv2.warpPerspective(raw_copy, M, (640,640), flags=cv2.INTER_LINEAR)
			mask = cv2.inRange(warped, lower, upper)
			insignia = cv2.bitwise_and(warped, warped, mask = mask)
			th_warp = cv2.adaptiveThreshold(cv2.cvtColor(insignia,cv2.COLOR_BGR2GRAY), 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 255, 1)
			conti,jerar = cv2.findContours(th_warp, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
			for cnt3 in conti:
				area3 = cv2.contourArea(cnt3)
				if ((area3 < 0.20*500000) and (area3 > 150)): #58250):
					peri3 = cv2.arcLength(cnt3, True)
					approx3 = cv2.approxPolyDP(cnt3, 0.02 * peri3, True)
					if len(approx3) == 4:
						x,y,w,h = cv2.boundingRect(approx3)
						cv2.rectangle(insignia,(x,y),(x+w,y+h),(20,200,255),1)

						alfa = 1.0*w/h 
						#print(w, h)
						if(alfa > 1):
							print("Corrigiendo perspectiva.")
							dst = np.array([[0,0],[int(fcam*640),0],[int(fcam*640),int(fcam*(640*alfa))],[0,int(fcam*(640*alfa))]], dtype="float32")
							M = cv2.getPerspectiveTransform(src3,dst)
							warped = cv2.warpPerspective(raw_copy, M, (int(fcam*640),int(fcam*(640*alfa))), flags=cv2.INTER_LINEAR)
							(alto_imagen, ancho_imagen, ch) = warped.shape
							# w es la cantidad de pixeles por centímetro.
							altoPlaca = 10.0*alto_imagen/w # Expresado en mílimetros, 10mm = 1cm.
							anchoPlaca = 10.0*ancho_imagen/w # Expresado en mílimetros, 10mm = 1cm.
							print("Alto: " + str(altoPlaca) + " Ancho: " + str(anchoPlaca))
						else:
							print("Corrigiendo perspectiva.")
							dst = np.array([[0,0],[int(fcam*(640/alfa)),0],[int(fcam*(640/alfa)),int(fcam*640)],[0,int(fcam*640)]], dtype="float32")
							M = cv2.getPerspectiveTransform(src3,dst)
							warped = cv2.warpPerspective(raw_copy, M, (int(fcam*(640/alfa)),int(fcam*640)))
							(alto_imagen, ancho_imagen, ch) = warped.shape
							# h es la cantidad de pixeles por centímetro.
							altoPlaca = 10.0*alto_imagen/h # Expresado en mílimetros, 10mm = 1cm.
							anchoPlaca = 10.0*ancho_imagen/h # Expresado en mílimetros, 10mm = 1cm.
							print("Alto: " + str(altoPlaca) + " Ancho: " + str(anchoPlaca))

			warped = cv2.flip(warped,1)
			cv2.imwrite("Warped.png",warped)
			ret = True

		elif(placaVisible is False):
			print("ERROR: No se encuentra placa impresa.")
			warped=np.zeros((1,1,3),dtype=np.uint8)
			cv2.imwrite("Warped.png",warped)
			ret = False
			altoPlaca = 0
			anchoPlaca = 0

		elif(insigniaVisible is False):		
			print("ERROR: No se encuentra insignia patrón.")
			warped=np.zeros((1,1,3),dtype=np.uint8)
			cv2.imwrite("Warped.png",warped)
			ret = False
			altoPlaca = 0
			anchoPlaca = 0

	return (ret, warped, altoPlaca, anchoPlaca)

