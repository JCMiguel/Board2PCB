#!/usr/bin/python2
# -*- coding: utf-8 -*-

# Este archivo contiene las funciones de análisis de la imagen, como ser la detección de pads y pistas.

import cv2
import numpy as np


######################## TITLE #############################
# 
#################################################################################
def MostrarResultado(plantilla, lista_islas, xi, yi, xf, yf, vertice_x, vertice_y, ancho_plano, alto_plano):
	print("Guardando resultados...")
	plantilla = cv2.cvtColor(plantilla,cv2.COLOR_GRAY2BGR)

	for i in range(1,len(xi)):
		cv2.line(plantilla,(xi[i],yi[i]),(xf[i],yf[i]),(200,12,255),1)

	for i in lista_islas[0,:]:
	    # draw the outer circle
	    cv2.circle(plantilla,(i[0],i[1]),i[2],(0,255,0),3)
	    # draw the center of the circle
	    cv2.circle(plantilla,(i[0],i[1]),2,(255,0,0),3)

	cv2.rectangle(plantilla,(vertice_x,vertice_y),(vertice_x+ancho_plano,vertice_y+alto_plano),(200,100,50),3)
	cv2.imwrite('PCB.png',plantilla)
#---------------------------------------------------------------------------------



######################## TITLE #############################
# 
#################################################################################
def GenerarLog(lista_islas, xi, yi, xf, yf, vertice_x, vertice_y, ancho_plano, alto_plano):
	print("Generando archivo Log.txt...")
	log = open("Log.txt", "w")
	
	log.write("PADS ENCONTRADOS\n---- -----------\n")
	log.write("\tCentro\t\t\tRadio\n")

	for i in lista_islas[0,:]:
		log.write("\t({0};{1})\t\t\t{2}\n".format(i[0],i[1],i[2]))

	log.write("\nPLANO DE MASA\n----- -- ----\n")
	log.write("\tOrigen\t\t\tAncho\t\t\tAlto\n")
	log.write("\t({0};{1})\t\t\t{2}\t\t\t{3}\n".format(vertice_x,vertice_y,ancho_plano,alto_plano))

	log.write("\nPISTAS DETECTADAS\n------ ----------\n")
	log.write("\tNro\t\t\tOrigen\t\t\tFinal\n")

	for i in range(1,len(xi)):
		log.write("\t{0}\t\t\t({1};{2})\t\t\t({3};{4})\n".format(i,xi[i],yi[i],xf[i],yf[i]))

	log.close()
#---------------------------------------------------------------------------------


