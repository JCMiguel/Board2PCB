#!/usr/bin/python2
# -*- coding: utf-8 -*-

# Notas para escalado.
#  La placa mide 15cm x 10cm
#  Resolucion maxima de disenio: 1mm
#	Escala de los pads: 2,040mm son 177 pixeles.
#	Escala de la placa en X: ancho_imagen / 150
#	Escala de la placa en Y: alto_image / 100


########################### Importo bibliotecas requeridas ###########################
print("Iniciando programa...")
import cv2
import numpy as np
import argparse
import capturaImagen as cImg
import preparaImagen as pImg
import analizaImagen as aImg
import informaResultado as iRes
#------------------------------------------------------------------------------------



########################### Defino argumentos posibles ##############################
parser = argparse.ArgumentParser(description="Board2PCB")
parser.add_argument("-v", "--videotouse", type=str, default="", help="Archivo AVI a utilizar. También se pueden especificar los números 0 y 1 para tomar lecturas desde las cámaras conectadas al equipo.")
parser.add_argument("-i", "--imagetouse", type=str, default="", help="Archivo de imagen a utilizar.")
parser.add_argument("-a", "--ancho", type=int, default=0, help="Ancho de la placa, expresado en milimetro [mm]. Se implementa únicamente si se especifica una imagen a utilizar.")
parser.add_argument("-l", "--largo",  type=int, default=0, help="Largo de la placa, expresado en milimetros [mm]. Se implementa únicamente si se especifica una imagen a utilizar.")
parser.add_argument("-p", "--preproceso", type=int, default=0, help="Aplica un umbral personalizado a la imagen antes de procesarla. El umbral debe ser un número entre 1 y 255.")
parser.add_argument("--debug", help="Salva los resultados parciales de cada etapa en formato PNG.", action="store_true")
#parser.add_argument("-l", "--longitudminima", type=int, default=1, help="Longitud minima de pista.")
#------------------------------------------------------------------------------------










########################### Aca empieza el script ###################################
if __name__ == "__main__" :
	args = parser.parse_args()
	if ((len(args.videotouse) == 0) and (len(args.imagetouse) == 0)):
		print("ERROR! Archivo no especificado.")
		exit()
	if ((len(args.imagetouse) != 0) and ((args.largo == 0) or (args.ancho == 0))):
		print("ERROR! Dimensiones de la placa no especificadas.")
		exit()
	if ((args.preproceso < 0) or (args.preproceso > 255)):
		print("ERROR! Valor de umbral de preprocesamiento fuera de rango.")
		exit()

	if (len(args.videotouse) != 0):
		ret, captura, altoPlaca, anchoPlaca = cImg.ObtieneImagen(args.videotouse)
		if(ret == 0):
			exit()
		imagen_gris = cv2.cvtColor(captura,cv2.COLOR_BGR2GRAY) 
	elif (len(args.imagetouse) != 0):
		imagen_gris = pImg.CargaImagen(args.imagetouse)
		if(imagen_gris.shape[0] > imagen_gris.shape[1]):
			altoPlaca = args.largo
			anchoPlaca = args.ancho
		else:
			altoPlaca = args.ancho
			anchoPlaca = args.largo

	(imagen_binaria, imagen_binaria_cuantificada) = pImg.PreparaImagen(imagen_gris, args.preproceso)

	#imagen_binaria_cuantificada = imagen_binaria.copy() ##### MAGIA!

	(escala_x, escala_y) = aImg.CalculaEscala(imagen_gris, altoPlaca, anchoPlaca)
	plantilla = pImg.CreaPlantilla(imagen_gris)
	aux, imagen_contornos, lista_contornos, lista_contornos_cuantificada = aImg.BuscaContornos(plantilla, imagen_binaria, imagen_binaria_cuantificada)


	if(args.preproceso == 0):
		lista_contornos_cuantificada = lista_contornos ##### MAGIA!

	lista_islas, debugBlur = aImg.DetectaPads(imagen_contornos)

	if(args.debug):
		print("DEBUG: Guardando resultados parciales")
		cv2.imwrite("DEBUG0_ImagenGris.png",imagen_gris)
		cv2.imwrite("DEBUG1_Threshold.png",imagen_binaria)
		cv2.imwrite("DEBUG2_ImagenCuantificada.png",imagen_binaria_cuantificada)
		cv2.imwrite("DEBUG3_Contornos.png",aux)	# Generada con la lista de contornos.
		cv2.imwrite("DEBUG4_ContornosErosionados.png",imagen_contornos)
		cv2.imwrite("DEBUG5_BlurGaussiano.png",debugBlur)

	if (lista_islas != None):
		xi,yi,xf,yf = aImg.DetectaPistas(lista_islas, lista_contornos_cuantificada, escala_x, escala_y, plantilla, imagen_binaria_cuantificada)
	vertice_x,vertice_y,ancho_plano,alto_plano = aImg.BuscaPlano(lista_contornos, imagen_binaria)
	iRes.MostrarResultado(plantilla, lista_islas, xi, yi, xf, yf, vertice_x, vertice_y, ancho_plano, alto_plano)
	iRes.GenerarLog(lista_islas, xi, yi, xf, yf, vertice_x, vertice_y, ancho_plano, alto_plano)
#------------------------------------------------------------------------------------
