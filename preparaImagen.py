#!/usr/bin/python2
# -*- coding: utf-8 -*-

# Este archivo contiene las funciones preparatorias de la imagen.

import cv2
import numpy as np


######################## Funcion de Carga de Imagen #############################
# Devuelve imagen en escala de grises
#################################################################################
def CargaImagen (archivo):
	print("Cargando imagen...")
	try:	
		imagen = cv2.imread(archivo)
	except:
		print("  ERROR! Verifique que el archivo exista.")
		exit()
	imagen_gris = cv2.cvtColor(imagen,cv2.COLOR_BGR2GRAY)
	return imagen_gris
#---------------------------------------------------------------------------------











##################### Funcion de preracion de imagen ############################
# Obtiene las imagenes binarias para procesamiento
#################################################################################
def PreparaImagen (imagen_gris, umbral_preproceso):
	print("Preparando imagen...")
	#imagen_gris = cv2.equalizeHist(imagen_gris) ## No fue más eficaz...

	# Obtengo imagen binaria
	#imagen_binaria = cv2.adaptiveThreshold(imagen_gris, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 255, 1)
	imagen_binaria = cv2.adaptiveThreshold(imagen_gris, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 255, 1)
	#ret,imagen_binaria = cv2.threshold(imagen_gris,0,255,cv2.THRESH_OTSU)


	# Cuantifico imagen en escala de grises	
	#Pproc_4color(imagen_gris)
	if(umbral_preproceso != 0):
		imagen_binaria_cuantificada = Pproc_Binario(imagen_gris, umbral_preproceso)
	else:
		imagen_binaria_cuantificada = imagen_binaria.copy()
	#ret,imagen_binaria = cv2.threshold(imagen_gris, 128, 255, cv2.THRESH_BINARY)

	return imagen_binaria, imagen_binaria_cuantificada


# Definiendo funciones que dependen de la anterior.
	########################### Preproceso de cambio de colores ###########################
	# Demora unos 4 minutos...
def Pproc_4color (imagen_ref):
	print("PREPROCESO: Cambiando colores...")
	imagen = np.copy(imagen_ref)
	(alto,ancho) = imagen.shape
	for y in range(alto):
		for x in range(ancho):
			g = imagen[y,x]
			imagen[y,x] = (g/64*64)#+32)
	return imagen
	#------------------------------------------------------------------------------------

	########################### Preproceso de cambio de colores ###########################
	# Demora unos 4 minutos...
def Pproc_Binario (imagen_ref, umbral):
	print("PREPROCESO: Binarizando...")
	imagen = np.copy(imagen_ref)
	(alto,ancho) = imagen.shape
	for y in range(alto):
		for x in range(ancho):
			g = imagen[y,x]
			if (g < umbral):
				imagen[y,x] = 0
			else:
				imagen[y,x] = 128
	return imagen
	#------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------











######################## Funcion para crear plantilla auxiliar #############################
# Crea una imagen vacia en color negro.
#################################################################################
def CreaPlantilla (imagen_ref):
	print("Creando plantilla auxiliar...")
	#ancho_imagen, alto_imagen = imagen_ref.shape[::-1]
	#plantilla = np.copy(imagen_ref)
	#for x in range(ancho_imagen):
	#	for y in range(alto_imagen):
	#		plantilla[y,x] = 0

	plantilla=np.zeros(imagen_ref.shape,dtype=np.uint8)


	return plantilla
#---------------------------------------------------------------------------------
